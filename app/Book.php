<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table= "books";
    protected $fillable = ["title", "author", "year", "description", "cover_book", "user_id"];
    protected $guarded = [];

    public function author(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
