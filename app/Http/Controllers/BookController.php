<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;



class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $book = Book::all();

        $user = Auth::user();
        $book = $user->books;

        return view('book.index', compact('book'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('book.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'title' => 'required',
            'author' => 'required',
            'year' => 'required',
            'description' => 'required',
            'cover_book' => 'required'
            
        ]);
        
        // $gambar = $request->cover_book;
        // $name_img = time(). ' - '.$gambar->getClientOriginalName();

       
       $book = Book::create([
            'title' => $request->title,
            'author' => $request->author,
            'year' => $request->year,
            'description' => $request->description,
            'cover_book' => $request->cover_book,
            'user_id' => Auth::id()
           
        ]);
        
        // $gambar->move('img', $name_img);
        Alert::success('Berhasil!!', 'Data Berhasil ditambah');
        return redirect('/book');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::find($id);

        return view('book.show', compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::find($id);

        return view('book.edit', compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'author' => 'required',
            'year' => 'required',
            'description' => 'required',
            'cover_book' => 'required'
            
        ]);

        $update = Book::where('id', $id)->update([
            "title" => $request["title"],
            "author" => $request["author"],
            "year" => $request["year"],
            "description" => $request["description"],
            "cover_book" => $request["cover_book"]
            
    ]);
    Alert::success('Berhasil!!', 'Data Berhasil dirubah');
    return redirect('/book');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
