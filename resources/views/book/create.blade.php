@extends('layouts.app')

@section('content')
<div class="ml-3 mt-3">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tambah Buku</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/book" method="POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Judul</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{old('title', '')}}" placeholder="Masukkan Judul Buku" required>
                    @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="author">Penulis</label>
                    <input type="text" class="form-control" id="author" name="author" value="{{old('author', '')}}" placeholder="Masukkan Nama Penulis" required>
                    @error('author')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="year">Tahun Terbit</label>
                    <input type="text" class="form-control" id="year" name="year" value="{{old('year', '')}}" placeholder="Masukkan Tahun Terbit" required>
                    @error('year')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="description">Deskripsi Buku</label>
                    <input type="text" class="form-control" id="description" name="description" value="{{old('description', '')}}" placeholder="Masukkan Deskripsi" required>
                    @error('description')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="cover_book">Cover Buku</label>
                    <input type="text" class="form-control" id="cover_book" name="cover_book" placeholder="Masukkan URL Gambar" required>
                    @error('cover_book')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>
</div>
@endsection