@extends('layouts.app')

@section('content')
<div class="ml-3 mt-3">
<div class="card card-primary ml-5 mr-5">
              <div class="card-header">
                <h3 class="card-title">Edit Buku {{$book->title}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/book/{{$book->id}}" method="POST">
              @csrf
              @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Judul</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{old('title', $book->title)}}" placeholder="Masukkan Judul Buku" required>
                    @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="author">Penulis</label>
                    <input type="text" class="form-control" id="author" name="author" value="{{old('author', $book->author)}}" placeholder="Masukkan Nama Penulis" required>
                    @error('author')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="year">Tahun Terbit</label>
                    <input type="text" class="form-control" id="year" name="year" value="{{old('year', $book->year)}}" placeholder="Masukkan Tahun Terbit" required>
                    @error('year')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="description">Deskripsi Buku</label>
                    <input type="text" class="form-control" id="description" name="description" value="{{old('description', $book->description)}}" placeholder="Masukkan Deskripsi" required>
                    @error('description')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="cover_book">Cover Buku</label>
                    <input type="text" class="form-control" id="cover_book" name="cover_book" value="{{old('cover_book', $book->cover_book)}}" placeholder="Masukkan URL Gambar" required>
                    @error('cover_book')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                  <a href="/book" class="btn btn-danger">Batal</a>
                </div>
              </form>
            </div>
</div>
@endsection