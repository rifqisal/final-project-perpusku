@extends('layouts.app')

@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-4">
                <div class="jumbotron jumbotron-fluid">
            <div class="container">
            <img class="card-img-top" src="{{$book->cover_book}}" alt="Card image cap">
            </div>
            </div>
    </div>
    <div class="col-md-8">
      
    <div class="card">
  <div class="card-header fw-bold">
    Detail Buku
  </div>
  <div class="card-body">
    <h5 class="card-title">Judul : {{$book->title}}</h5>
    <p class="card-text">Penulis : {{$book->author}}</p>
    <p class="card-text">Tahun Terbit : {{$book->year}}</p>
    <p class="card-text">Deskripsi : {{$book->description}}</p>
    
     <a href="/book/{{$book->id}}/edit" class="btn btn-success">Edit</a>
  <a href="/book" class="btn btn-danger">Kembali</a>
  </div>
 
</div>


    </div>
</div>

@endsection