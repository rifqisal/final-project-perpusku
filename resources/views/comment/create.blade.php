@extends('layouts.app')
@section('content')
<div class="ml-3 mt-3">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Berikan Komentar!</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/comment" method="POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Nama</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{old('name', '')}}" placeholder="Masukkan Nama" required>
                    @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="text">Komentar</label>
                    <input type="text" class="form-control" id="text" name="text" value="{{old('text', '')}}" placeholder="Masukkan komentar" required>
                    @error('text')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>
</div>
@endsection