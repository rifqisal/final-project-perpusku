@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-4">
                <div class="jumbotron jumbotron-fluid">
            <div class="container">
            <img class="card-img-top" src="{{$book->cover_book}}" alt="Card image cap">
            </div>
            </div>
    </div>
    <div class="col-md-8">
      
    <div class="card">
  <div class="card-header fw-bold">
    Detail Buku
  </div>
  <div class="card-body">
    <h5 class="card-title">Judul : {{$book->title}}</h5>
    <p class="card-text">Penulis : {{$book->author}}</p>
    <p class="card-text">Tahun Terbit : {{$book->year}}</p>
    <p class="card-text">Deskripsi : {{$book->description}}</p>
    
     
  <a href="/book" class="btn btn-danger">Kembali</a>
  </div>
 
</div>


    </div>
</div>

<div class="container ml-5 mr-5 mt-5">
<div class="mt-3 ml-3">

<div class="card">
              <div class="card-header">
                <h3 class="card-title">Komentar</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
              @endif
              
              
              @foreach($comment as $key => $comment)
                <div class="card mt-2 ml-5 mr-5">                             
                        <div class="card-header ">
                        {{ $comment -> name }}
                        </div>
                        <div class="card-body">
                          <h5 class="card-title">{{ $comment -> text }}</h5>
                        </div>                       
                      </div>
                      @endforeach                     
              </div>                                      
              

            </div>
</div>

<div class="ml-3 mt-3">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Berikan Komentar!</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/comment" method="POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <!-- <label for="name">Nama</label> -->
                    <input type="text" class="form-control" id="name" name="name" value="{{old('name', '')}}" placeholder="Masukkan Nama" required>
                    @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <!-- <label for="text">Komentar</label> -->
                    <input type="text" class="form-control" id="text" name="text" value="{{old('text', '')}}" placeholder="Masukkan komentar" required>
                    @error('text')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Buat</button>
                </div>
              </form>
            </div>
</div>
</div>
@endsection