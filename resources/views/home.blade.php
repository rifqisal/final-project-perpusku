@extends('layouts.app')

@section('content')
<div class="ml-5 mr-5 mt-5">

<h2 align="center">Buku Kita</h2>
<div class="row">
@foreach($book as $key => $book)
<div class="col-3 mt-3">
<div class="card" style="width: 16rem;">
  <img class="card-img-top" src="{{$book->cover_book}}" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">{{$book->title}}</h5>
    
    <a href="/comment/{{$book->id}}" class="btn btn-primary">Detail</a>
  </div>
</div>

</div>
@endforeach
</div>
</div>
@endsection
