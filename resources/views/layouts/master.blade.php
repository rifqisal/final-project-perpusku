<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" href="../../css/style.css">
    <title>@yield('Header')</title>
</head>
<body>
    <div class="container">
            <div>
                    <nav class="mb-20 mt-3 d-flex justify-content-between" style="">
                        <a href="">
                            <img src="../../img/perpusku.png" width="22%" alt="">
                        </a>
                        <ul class="nav justify-content-lg-center " >
                        <li class="nav-item">
                            <a class="nav-link fw-bold" aria-current="page" href="login" id="as">Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link fw-bold" href="register" id="as">Register</a>
                        </li>
                        </ul>
                    </nav>
            </div> 
            <div>
                @yield('content')
            </div> 
    </div>
</body>
</html>