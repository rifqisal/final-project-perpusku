@extends('layouts.app')
@section('Header')
    Beranda
@endsection
@section('content')
    <div class="row">
        <div class="col-8 text-center">
            {{----}}
            <div>
                <h1 class="mt-5 ">Selamat Datang <br>di Perpusku.</h1>
            </div>
            <div>
                <img src="{{asset('/img/duduk.png')}}" width="80%" alt="">
            </div>
        </div>
        <div class="col-4">
            {{----}}
            <div class="mt-5">
                <h1>Ayo Tingkatkan Literasi</h1>
                <h3>Bersama Kami!</h3>
            </div>
            <div class="">
                <div class="mt-5 d-flex">
                    <img src="{{asset('/img/loupe.png')}}" width="10%" alt="">
                    
                    <h4>Cari buku yang kamu suka</h4>
                </div>
                <div class="mt-5 d-flex">
                    <img src="{{asset('/img/free.png')}}" width="10%" alt="">
                    <h4>Gratis Selamanya</h4>
                </div>
                <div class="mt-5 d-flex">
                    <img src="{{asset('/img/medal.png')}}" width="10%" alt="">
                    
                    <h4>Buku berkualitas</h4>
                </div>
            </div>
        </div>
    </div>
@endsection